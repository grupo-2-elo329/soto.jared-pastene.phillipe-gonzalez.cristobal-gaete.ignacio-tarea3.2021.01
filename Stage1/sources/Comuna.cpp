/* Qt libs */
#include <QRect>

/* C++ libs */
#include <iostream>
using namespace std;

/* Dev libs */
#include "Comuna.hpp"

/* Constructor */
Comuna::Comuna(QObject* parent,int width,int length,double speed, double dTheta,int radius):QGraphicsScene(parent){
	this->width=width;
	this->length=length;
    this->person = new Individuo(width,length,speed,dTheta,radius);
    this->addItem(this->person);
}

/* Destructor */
Comuna::~Comuna(){if(this->person!=NULL) delete this->person;}

/* Getters */
int Comuna::getWidth(){return this->width;}

int Comuna::getLength(){return this->length;}

int Comuna::getPersonX(){return this->person->getX();}

int Comuna::getPersonY(){return this->person->getY();}

double Comuna::getPersonSpeed(){return this->person->getSpeed();}

double Comuna::getPersonDtheta(){return this->person->getDtheta();}

double Comuna::getPersonTheta(){return this->person->getTheta();}

/* Printers */
void Comuna::printPersonValues(){this->person->printAll();}

/* State related */
void Comuna::computeNextState(){this->person->computeNextPos();}

void Comuna::updateState(){
    this->person->updatePos();
    this->update(-this->width/2,-this->length/2,this->width,this->length);
}

/* Updaters */
void Comuna::updatePersonSpeed(double speed){this->person->setSpeed(speed);}

void Comuna::updatePersonRadius(int radius){this->person->setRadius(radius);}
