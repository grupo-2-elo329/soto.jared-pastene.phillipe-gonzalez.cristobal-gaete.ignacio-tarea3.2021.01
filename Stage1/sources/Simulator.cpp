/* Qt libs */
#include <QMenuBar>

/* C++ libs */
#include <iostream>
#include <ctime>
#include <cmath>
using namespace std;

/* Dev libs */
#include "Simulator.hpp"
#include "ConfigFile.hpp"
#include "Comuna.hpp"

/* Constructor */
Simulator::Simulator(QWidget* parent,ConfigFile* config):QMainWindow(parent),config(config){
    this->setupView();
    this->setupMenuBar();
    this->firstTimeSim = true;
    this->isOn = false;
}

/* Destructor */
Simulator::~Simulator(){
	if(this->config!=NULL){delete this->config;}
	if(this->valpo!=NULL){delete this->valpo;}
	if(this->controlMenu!=Q_NULLPTR){delete this->controlMenu;}
	if(this->startAction!=Q_NULLPTR){delete this->startAction;}
	if(this->stopAction!=Q_NULLPTR){delete this->stopAction;}
	if(this->settingsAction!=Q_NULLPTR){delete this->settingsAction;}
    if(this->timer!=Q_NULLPTR){delete this->timer;}
    if(this->view!=Q_NULLPTR){delete this->view;}
}

/* Setups */
void Simulator::setupMenuBar(){
	this->setWindowTitle("Simulador Gráfico Pandemia 2021");
    this->setFixedSize(this->config->getWidth()+20,this->config->getLength()+this->menuBar()->height()+20);
	this->controlMenu = new QMenu("Control",this);
	this->startAction = new QAction("Start",this);
	this->stopAction = new QAction("Stop",this);
    this->settingsAction = new QAction("Settings",this);
	this->controlMenu->addAction(this->startAction);
	this->controlMenu->addAction(this->stopAction);
	this->menuBar()->addMenu(this->controlMenu);
	this->menuBar()->addAction(this->settingsAction);
    this->timer = new QTimer(this);
    this->timer->setInterval((int)(1000*this->config->getDt()));
    /* Connections to MenuBar */
    connect(this->startAction,SIGNAL(triggered()),this,SLOT(initSlot()));
    connect(this->timer,SIGNAL(timeout()),this,SLOT(startSlot()));
    connect(this->stopAction,SIGNAL(triggered()),this->timer,SLOT(stop()));
    connect(this->stopAction,SIGNAL(triggered()),this,SLOT(stopSlot()));
    connect(this->settingsAction,SIGNAL(triggered()),this,SLOT(settingsSlot()));
    /* Connections to Config Popup */
    connect(this->config,SIGNAL(accepted()),this,SLOT(updateSimConfig()));
}

void Simulator::setupView(){
    this->valpo = new Comuna(this,this->config->getWidth(),this->config->getLength(),this->config->getSpeed(),this->config->getDtheta(),this->config->getRadius());
    this->valpo->setSceneRect(-this->config->getWidth()/2,-this->config->getLength()/2,this->config->getWidth(),this->config->getLength());
    this->valpo->addRect(-this->config->getWidth()/2,-this->config->getLength()/2,this->config->getWidth(),this->config->getLength());
    this->view = new QGraphicsView(this->valpo,this);
    this->view->setFixedSize(this->config->getWidth()+15,this->config->getLength()+15);
    this->view->setRenderHint(QPainter::Antialiasing);
    this->view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->setCentralWidget(this->view);
}

/* State related */
void Simulator::computeNextSimState(){this->valpo->computeNextState();}

void Simulator::updateSimState(){
	this->valpo->updateState();
	this->simTime += (int)(1000*this->config->getDt());
}

/* Printers */
void Simulator::printSimState(){
    cout <<  this->simTime << ",\t" << this->valpo->getPersonX() << ",\t" << this->valpo->getPersonY() << ",\t" << (int)((180.0/M_PI)*this->valpo->getPersonTheta()) << ",\t" << (int)((180.0/M_PI)*this->valpo->getPersonDtheta()) << endl;
}

/* Slots */
void Simulator::initSlot(){
    if((not this->isOn) and this->config->isHidden()){
        connect(this->startAction,SIGNAL(triggered()),this->timer,SLOT(start()));
        this->isOn = true;
        this->simTime = 0;
        if(this->firstTimeSim){
            this->firstTimeSim = false;
            cout << "t[ms],\t" << "x,\t" << "y,\t" << "theta,\t" << "dTheta" << endl;
        }
        this->timer->start();
        this->printSimState();
    }
}

void Simulator::startSlot(){
    if(this->config->isHidden()){
        this->computeNextSimState();
        this->updateSimState();
        this->printSimState();
    }
}

void Simulator::stopSlot(){this->isOn = false;}

void Simulator::settingsSlot(){
    if(not this->isOn){
        disconnect(this->startAction,SIGNAL(triggered()),this->timer,SLOT(start()));
        this->config->show();
    }
}

void Simulator::updateSimConfig(){
    this->config->updateSpinnerValues();
    this->valpo->updatePersonSpeed(this->config->getSpeed());
    this->valpo->updatePersonRadius(this->config->getRadius());
}
