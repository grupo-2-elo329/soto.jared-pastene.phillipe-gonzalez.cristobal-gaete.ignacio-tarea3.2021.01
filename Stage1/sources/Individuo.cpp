/* C++ libs */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;

/* Dev libs */
#include "Individuo.hpp"

/* Constructor */
Individuo::Individuo(int comunaWidth,int comunaLength,double speed,double dTheta,int radius){
	srand(time(NULL));
    this->comunaWidth = comunaWidth;
    this->comunaLength = comunaLength;
    this->x = radius+rand()%(comunaWidth-2*radius);
    this->y = radius+rand()%(comunaLength-2*radius);
	this->speed = (0.9+0.2*rand()/RAND_MAX)*speed;
	this->dTheta = dTheta;
	this->theta = 2.0*M_PI*rand()/RAND_MAX;
    this->radius = radius;
}

/* Destructor */
Individuo::~Individuo(){}

/* Getters */
int Individuo::getX(){return this->x;}

int Individuo::getY(){return this->y;}

double Individuo::getSpeed(){return this->speed;}

double Individuo::getDtheta(){return this->dTheta;}

double Individuo::getTheta(){return this->theta;}

/* Setters */
void Individuo::setSpeed(double speed){this->speed=speed;}

void Individuo::setRadius(int radius){this->radius=radius;}

/* Printers */
void Individuo::printAll(){
	cout << "Printing person values..." << endl;
	cout << "x\t" << this->x << endl;
	cout << "y\t" << this->y << endl;
	cout << "speed\t" << this->speed << endl;
	cout << "dTheta\t" << this->dTheta << endl;
	cout << "theta" << this->theta << endl << endl;
}

/* State related */
void Individuo::computeNextPos(){
	double thetaNextTemp = this->theta+(2.0*rand()/RAND_MAX-1.0)*this->dTheta;
	int xNextTemp = (int)(this->x+(this->speed)*cos(thetaNextTemp));
	int yNextTemp = (int)(this->y+(this->speed)*sin(thetaNextTemp));
    if(xNextTemp<this->radius){
        xNextTemp = 2*this->radius-xNext;
        thetaNextTemp = M_PI-thetaNextTemp;
    }
    if(xNextTemp>this->comunaWidth-this->radius){
        xNextTemp = 2*(this->comunaWidth-this->radius)-xNextTemp;
        thetaNextTemp = M_PI-thetaNextTemp;
    }
    if(yNextTemp<this->radius){
        yNextTemp = 2*this->radius-yNextTemp;
        thetaNextTemp = -thetaNextTemp;
    }
    if(yNextTemp>this->comunaLength-this->radius){
        yNextTemp = 2*(this->comunaLength-this->radius)-yNextTemp;
        thetaNextTemp = -thetaNextTemp;
    }
    this->xNext = xNextTemp;
    this->yNext = yNextTemp;
    this->thetaNext = thetaNextTemp;//remainder(thetaNextTemp,2.0*M_PI);
}

void Individuo::updatePos(){
	this->x = this->xNext;
	this->y = this->yNext;
    this->theta = this->thetaNext;
}

/* Overrides */
QRectF Individuo::boundingRect()const{return QRectF(-this->radius,-this->radius,2*this->radius,2*this->radius);}

void Individuo::paint(QPainter* painter,const QStyleOptionGraphicsItem*,QWidget*){
    painter->setBrush(QColor(255,0,0));//rgb
    painter->drawEllipse(this->x-this->comunaWidth/2-this->radius,this->y-this->comunaLength/2-this->radius,2*this->radius,2*this->radius);
}
