#ifndef COMUNA_HPP
#define COMUNA_HPP
/* Qt libs */
#include <QGraphicsScene>

/* Dev libs */
#include "Individuo.hpp"

class Comuna : public QGraphicsScene{
    Q_OBJECT
	private:
		int width,length;
		Individuo* person;
	public:
        Comuna(QObject*,int,int,double,double,int);
		~Comuna();
		int getWidth();
		int getLength();
		int getPersonX();
		int getPersonY();
		double getPersonSpeed();
		double getPersonDtheta();
		double getPersonTheta();
		void printPersonValues();
		void computeNextState();
        void updateState();
        /* Updaters */
        void updatePersonSpeed(double);
        void updatePersonRadius(int);
};

#endif
