#ifndef INDIVIDUO_HPP
#define INDIVIDUO_HPP

#include <QGraphicsItem>
#include <QPainter>

class Individuo : public QGraphicsItem{
	private:
		int x,xNext;
		int y,yNext;
        int comunaWidth,comunaLength;
		double speed,dTheta;
		double theta,thetaNext;
        int radius;
	public:
        Individuo(int,int,double,double,int);
		~Individuo();
        /* Overrides */
        QRectF boundingRect() const;
        void paint(QPainter *painter,const QStyleOptionGraphicsItem *option,QWidget *widget);
		/* Getters */
		int getX();
		int getY();
		double getSpeed();
		double getDtheta();
		double getTheta();
        /* Setters */
        void setSpeed(double);
        void setRadius(int);
        /* Printers */
        void printAll();
        /* State related */
        void computeNextPos();
        void updatePos();
};

#endif
