#ifndef SIMULATOR_HPP
#define SIMULATOR_HPP

/* Qt libs */
#include <QMainWindow>
#include <QTimer>
#include <QGraphicsView>

/* Dev libs */
#include "Comuna.hpp"
#include "ConfigFile.hpp"

class Simulator : public QMainWindow{
	Q_OBJECT
	private:
		ConfigFile* config;
		Comuna* valpo;
		QMenu* controlMenu;
		QAction* startAction;
		QAction* stopAction;
		QAction* settingsAction;
		QTimer* timer;
        QGraphicsView* view;
		int simTime;
        bool firstTimeSim;
        bool isOn;
	public:
		Simulator(QWidget* parent=Q_NULLPTR,ConfigFile* config=NULL);
		~Simulator();
		void setupMenuBar();
        void setupView();
		void computeNextSimState();
		void updateSimState();
		void printSimState();
	public slots:
		void initSlot();
		void startSlot();
		void stopSlot();
        void settingsSlot();
        void updateSimConfig();
};

#endif
