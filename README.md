# [ELO329] [Tarea 3: Simulandor Gráfico de la Evolución de una Pandemia](https://gitlab.com/grupo-2-elo329/soto.jared-pastene.phillipe-gonzalez.cristobal-gaete.ignacio-tarea3.2021.01/)

## Integrantes
* [Jared Soto](https://gitlab.com/Jared677)
* [Philippe Pastene](https://gitlab.com/Philippe_)
* [Cristobal Gonzalez](https://gitlab.com/Cristobal.Gonzalezs)
* [Ignacio Gaete](https://gitlab.com/IgnacioGaete)

## Objetivo General

Esta tarea busca rehacer la [Tarea 2](https://gitlab.com/grupo-2-elo329/tarea_2) pero utilizando QT en C++. De ninguna manera la intención es obtener resultados sobre cómo debería evolucionar una pandemia real. Además, otros objetivos de este proyecto son:

* Ejercitar la compilación y ejecución de programas en lenguaje C++ desde una consola de comandos.
* Ejercitar la configuración de un ambiente de trabajo para desarrollar aplicaciones en lenguaje C++ (udo del IDE QtCreator).
* Reconocer clases y relaciones entre ellas en lenguaje C++.
* Diseño y programación de clases en C++.
* Ejercitar la entrada de datos en C++.
* Ejercitar la preparación y entrega de resultados de software (creación de makefiles, readme, documentación, manejo de repositorio [git](https://git-scm.com/)).
* Desarrollar aplicaciones gráficas.
* Uso de metodología de desarrollo "iterativa" e "incremental".

## Descripción de la Tarea

Se considera un número dado de **N** individuos representados por la clase *Individuo* dispersos que se mueven aleatoriamente y de manera confinada en una región plana rectangular llamada *Comuna* durante un tiempo **T** (tiempo de simulación). De los **N** individuos iniciales, **S** es el número de individuos susceptibles a infectarse e **I** es el número de individuos ya infectados. En este ejercicio los individuos infectados pasan a estado **R** (recuperados) al cabo de **I_time** segundos.

La *Comuna* es rectangular de dimensiones (**width**,**length**) y los movimientos de los individuos son aleatorios con rapidez seleccionada entre 0.9***Speed** y 1.1***Speed** (así se simulan individuos caminando calmados y otros ágiles o más jóvenes). La dirección **theta** de la velocidad varóa cada **dt** a un valor aleatorio tomado de entre **theta-dTheta** y **theta+dTheta**. Cuando un *Individuo* tiende a salir de la *Comuna*, éste cambia su dirección como si estuviera rebotando en el borde de ésta.

Cuando un *Individuo* susceptible de infectarse se acerca a una distancia inferior a **d** [m] de uno infectado, existe una probabilidad P de que adquiera la infección por cada segundo en contacto cercano. Esta probabilidad de contagio depende de si ningun (P=**p0**), solo uno (P=**p1**) o ambos (P=**p2**) usan mascarilla. El parámetro **M** indica la fracción de individuos de cada tipo que usan mascarilla. Por ejemplo, si **M**=0.2, significa que 1/5 de los individuos infectados usan mascarilla y 1/5 de los susceptibles de infectarse también la usan. **M** es otro parámetro de la simulación ingresado por archivo.

Finalmente, en la *Comuna* se activan **NumVac** *Vacunatorios* a los **VacTime** segundos de iniciada la simulación. Los *Vacunatorios* son representados como zonas cuadradas fijas de lado **VacSize** y ubicados aleatoriamente en la comuna. Cuando un *Individuo* susceptible ingresa al área de un *Vacunatorio*, éste es vacunado y deja de ser susceptible a la infección. *Individuos* infectados o recuperados no son vacunados en esta simulación.

## [Stage 1](https://gitlab.com/grupo-2-elo329/soto.jared-pastene.phillipe-gonzalez.cristobal-gaete.ignacio-tarea3.2021.01/-/tree/master/Stage1): Individuo se mueve aleatoriamente
En esta etapa no hay vacunatorios, no se usa mascarilla y solo existe un *Individuo* que se mueve aleatoriamente en la *Comuna*. Las clases principales aquí son *Stage1*, *Simulador*, *Comuna* e *Individuo*.

## [Stage 2](https://gitlab.com/grupo-2-elo329/soto.jared-pastene.phillipe-gonzalez.cristobal-gaete.ignacio-tarea3.2021.01/-/tree/master/Stage2): Varios Individuos se mueven y se contagian
Esta etapa es similar a la previa (sin vacuna, sin mascarilla), excepto que se crean **N** individuos los cuales son almacenados en un arreglo. La clase *Comuna* crea y ubica **I** individuos infectados y (**N**-**I**) individuos susceptibles de infectarse. La clase *Individuo* se completa para reflejar la interacción entre individuos.

## [Stage 3](https://gitlab.com/grupo-2-elo329/soto.jared-pastene.phillipe-gonzalez.cristobal-gaete.ignacio-tarea3.2021.01/-/tree/master/Stage3): Alguno de los Individuos pueden usar mascarilla
Esta etapa modifica la forma como los individuos se pueden contagiar dependiendo del uso de mascarilla, implementada como un atributo extra en la clase *Individuo*. Además se incorpora a la interfaz un gráfico de áreas apiladas que registra la eveolución de las variables de interés (**S**, **I** y **R**).

## [Stage 4](https://gitlab.com/grupo-2-elo329/soto.jared-pastene.phillipe-gonzalez.cristobal-gaete.ignacio-tarea3.2021.01/-/tree/master/Stage4): Se crean Vacunatorios que inmunizan a Individuos de ser contagiados
En esta última etapa se logra incorporar todas las funcionalidades pedidas, es decir, individuos moviéndose aleatoriamente en la comuna, unos con mascarillas y otros no, se contagian entre ellos y además al cabo de un tiempo **VacTime** se generan **NumVac** vacunatorios los cuales vacunan aquellos individuos susceptibles que esten dentro de su región. En el gráfico también se reflejan esto cambios

## Interfaz gráfica
![Stage4](Stage4/stage4.png)
La interfaz gráfica implementada en la última etapa consiste en dos partes: a la izquierda de puede visualizar el movimiento de los distintos individuos en la región rectangular con distintos colores (*VERDE* para susceptibles, *ROJO* para infectados, *CAFÉ* para recuperados y *AMARILLO* para vacunados) y bordes (aquellos con borde negro utilizan mascarilla y aquellos que no tengan bordes no usan) y la parte derecha reservada para generar el gráfico de las variables de interés el cula se actualiza en cada iteración.

Además, como extra existe la posibilidad de acelerar o ralentizar la simulación con las flechas *IZQUIERDA* y *DERECHA* del teclado (solo cuando la simulación comienza), así como una opción en el menú "Settings" que permite visualizar la distancia mínima de contagio para aquellos individuos susceptibles de infectarse.

## Archivo de entrada **input.txt**
Cada etapa requiere de un archivo de entrada (**input.txt**) donde se establecen los parámetros de simulación. La estructura de este archivo es la siguiente:

**T**<*espacio*>**N**<*espacio*>**I**<*espacio*>**I_Time**

**width**<*espacio*>**length**

**speed**<*espacio*>**dt**<*espacio*>**dTheta**

**d**<*espacio*>**M**<*espacio*>**p0**<*espacio*>**p1**<*espacio*>**p2**

**NumVac**<*espacio*>**VacSize**<*espacio*>**VacTime**<*espacio*>

Si bien la estructura es rígida, el valor de cada parámetro puede ser cambiado sin ningun problema (**deben estar todos definidos incluso si su valor fuera cero**).

## Archivo de salida **output.csv**
Éste archivo es generado automáticamente en las etapas 2,3 y 4 y permite obtener los datos de la última simulación hecha ordenados en distintas líneas y separados por comas entre ellos. Éste archivo se puede por ejemplo abrir con Excel y manipuarlos acorde a lo que sea necesario.

## Sobre la ejectución de cada etapa del proyecto
El requisito principal es contar con al menos Qt Creator 4.15.2, además de módulos escenciales "Desktop gcc 64-bit" y "Qt Charts" ambos al menos de la versión 5.12.11. Cumplidos estos requisitos, basta con configurar el archivo de entrada input.txt y ejecutar en Qt Creator el proyecto completo.
