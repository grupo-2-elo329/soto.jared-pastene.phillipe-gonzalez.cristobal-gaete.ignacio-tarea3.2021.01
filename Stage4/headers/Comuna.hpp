#ifndef COMUNA_HPP
#define COMUNA_HPP
/* Qt libs */
#include <QGraphicsScene>
#include <QList>
#include <QChart>
#include <QAreaSeries>
#include <QLineSeries>
#include <QChartView>
using namespace QtCharts;

/* Dev libs */
#include "Individuo.hpp"
#include "Vacunatorio.hpp"

class Comuna : public QGraphicsScene{
    Q_OBJECT
	private:
		int width,length;
        int S,I,R,V;
        double D;
        double ownDt;
        int ownSimTime;
        QList<Individuo*> persons;
        QList<Vacunatorio*> vacs;
        QLineSeries *serieS, *serieI, *serieR, *serieV;
        QAreaSeries *areaS, *areaI, *areaR, *areaV;
        QChart *chart;
        QChartView *chartView;
	public:
        Comuna(QObject*,int,int,int,int,double,double);
		~Comuna();
		int getWidth();
        int getLength();
        QString getDataQString();
        double getP0();
        int getOwnSimTime();
        /* State related */
        void computeNextState();
        void updateState(int);
        int infectedRadar(int,int);
        int maskRadar(int,int);
        bool isInsideAnyVac(int,int);
        /* Printers */
        void printState();
        void initPersons(double,double,int,int,int,double,double,double,double,double,double,bool);
        void initVacs(int,int);
        void deleteVacs();
        void clearData();
        /* Updaters */
        void updateProbs(double,double,double);
};

#endif
