#ifndef INDIVIDUO_HPP
#define INDIVIDUO_HPP

#include <QGraphicsItem>
#include <QPainter>

#define SUSCEPTIBLE 0
#define INFECTADO   1
#define RECUPERADO  2
#define VACUNADO    3

class Individuo : public QGraphicsItem{
	private:
		int x,xNext;
		int y,yNext;
        int state,stateNext;
        int comunaWidth,comunaLength;
		double speed,dTheta;
		double theta,thetaNext;
        int radius;
        int Itime,initItime;
        double p0,p1,p2;
        double D;
        bool wearingMask,showD;
	public:
        Individuo(int,int,double,double,int,int,double,double,double,double,double,bool,bool);
		~Individuo();
        /* Overrides */
        QRectF boundingRect() const;
        void paint(QPainter *painter,const QStyleOptionGraphicsItem *option,QWidget *widget);
		/* Getters */
		int getX();
		int getY();
		double getSpeed();
		double getDtheta();
		double getTheta();
        int getRadius();
        int getState();
        double getP0();
        /* Setters */
        void setSpeed(double);
        void setRadius(int);
        /* Printers */
        void printAll();
        /* State related */
        void computeNextPos();
        void computeNextState(int,int,int,bool);
        void updatePos();
        void updateState(int);
		/* booleans */
		bool isWearingMask();
        /* Updaters */
        void updateProbs(double,double,double);
};

#endif
