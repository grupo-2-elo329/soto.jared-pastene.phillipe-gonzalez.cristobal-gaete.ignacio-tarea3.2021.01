#ifndef VACUNATORIO_HPP
#define VACUNATORIO_HPP

#include <QGraphicsItem>
#include <QPainter>

class Vacunatorio : public QGraphicsItem{
    private:
        int x,y;
        int size;
        int comunaWidth,comunaLength;
    public:
        Vacunatorio(int,int,int);
        ~Vacunatorio();
        /* Getters */
        int getX();
        int getY();
        int getSize();
        /* Booleans */
        bool isInside(int,int);
        /* Overrides */
        QRectF boundingRect() const;
        void paint(QPainter *painter,const QStyleOptionGraphicsItem *option,QWidget *widget);

};

#endif
