/* Qt libs */
#include <QRect>
#include <QChart>
#include <QAreaSeries>
#include <QLineSeries>
#include <QChartView>

/* C++ libs */
#include <iostream>
#include <ctime>
using namespace std;

/* Dev libs */
#include "Comuna.hpp"

/* Constructor */
Comuna::Comuna(QObject* parent,int width,int length,int N,int I,double D,double Dt):QGraphicsScene(parent){
    srand(time(NULL));
	this->width=width;
	this->length=length;
	this->S=N-I;
	this->I=I;
    this->R=0;
    this->V=0;
    this->D=D;
    this->ownDt=Dt;
    this->ownSimTime = 0;
    this->serieS = new QLineSeries();
    this->serieI = new QLineSeries();
    this->serieR = new QLineSeries();
    this->serieV = new QLineSeries();
    this->areaS = new QAreaSeries(serieS);
    this->areaI = new QAreaSeries(serieI, serieS);
    this->areaR = new QAreaSeries(serieR, serieI);
    this->areaV = new QAreaSeries(serieV, serieR);
    this->chart = new QChart();
    this->chart->createDefaultAxes();
    this->chart->addSeries(areaS);
    this->chart->addSeries(areaI);
    this->chart->addSeries(areaR);
    this->chart->addSeries(areaV);
    this->chartView = new QChartView(chart);
    this->addWidget(this->chartView);
    this->chartView->setFixedSize(width-20,length);
    this->chartView->move(0,-length/2);
    chart->setTitle("Evolución de la pandemia");
    areaS->setName("SUSCEPTIBLES");
    areaI->setName("INFECTADOS");
    areaR->setName("RECUPERADOS");
    areaV->setName("VACUNADOS");
    QLinearGradient gradient(QPointF(0, 0), QPointF(0, 1));
    gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    gradient.setColorAt(0.0, 0x006600);
    gradient.setColorAt(1.0, 0x33ff33);
    areaS->setBrush(gradient);
    gradient.setColorAt(0.0, 0x990000);
    gradient.setColorAt(1.0, 0xFF0000);
    areaI->setBrush(gradient);
    gradient.setColorAt(0.0, 0x462000);
    gradient.setColorAt(1.0, 0x964B00);
    areaR->setBrush(gradient);
    gradient.setColorAt(0.0, 0xffe505);
    gradient.setColorAt(1.0, 0xfffb05);
    areaV->setBrush(gradient);
    chart->createDefaultAxes();
}

/* Destructor */
Comuna::~Comuna(){}

/* Getters */
int Comuna::getWidth(){return this->width;}

int Comuna::getLength(){return this->length;}

QString Comuna::getDataQString(){
    QString result = "";
    result+=QString::number(this->S);
    result+=",";
    result+=QString::number(this->I);
    result+=",";
    result+=QString::number(this->R);
    result+=",";
    result+=QString::number(this->V);
    result+="\n";
    return result;
}

double Comuna::getP0(){return this->persons.first()->getP0();}

int Comuna::getOwnSimTime(){return this->ownSimTime;}

/* State related */
void Comuna::computeNextState(){
    int xTemp,yTemp;
	for(int i=0;i<this->persons.size();i++){
		this->persons.at(i)->computeNextPos();
		xTemp = this->persons.at(i)->getX();
        yTemp = this->persons.at(i)->getY();
        this->persons.at(i)->computeNextState(this->ownSimTime,this->infectedRadar(xTemp,yTemp),this->maskRadar(xTemp,yTemp),this->isInsideAnyVac(xTemp,yTemp));
	}
}

void Comuna::updateState(int simTime){
	this->S=0;
	this->I=0;
	this->R=0;
    this->V=0;
	for(int i=0;i<this->persons.size();i++){
		this->persons.at(i)->updatePos();
		this->persons.at(i)->updateState(simTime);
        this->S+=(this->persons.at(i)->getState()==SUSCEPTIBLE)? 1 : 0;
		this->I+=(this->persons.at(i)->getState()==INFECTADO)? 1 : 0;
		this->R+=(this->persons.at(i)->getState()==RECUPERADO)? 1 : 0;
        this->V+=(this->persons.at(i)->getState()==VACUNADO)? 1 : 0;
	}
    this->update(-this->width/2,-this->length/2,this->width,this->length);
    *serieS << QPointF(this->ownSimTime, S);
    *serieI << QPointF(this->ownSimTime, I+S);
    *serieR << QPointF(this->ownSimTime, R+I+S);
    *serieV << QPointF(this->ownSimTime, R+I+S+V);
    chart->axes(Qt::Horizontal).first()->setRange(0, this->ownSimTime);
    this->ownSimTime+=(int)(1000*this->ownDt);

}

int Comuna::infectedRadar(int x, int y){
	int counter=0;
	for(int i=0;i<this->persons.size();i++){
        if(x!=this->persons.at(i)->getX() && y!=this->persons.at(i)->getY() && this->persons.at(i)->getState()==INFECTADO){
			counter+=((x-this->persons.at(i)->getX())*(x-this->persons.at(i)->getX())+(y-this->persons.at(i)->getY())*(y-this->persons.at(i)->getY())<(int)((this->D+this->persons.at(i)->getRadius())*(this->D+this->persons.at(i)->getRadius()))) ? 1 : 0;
		}
	}
	return counter;
}

int Comuna::maskRadar(int x, int y){
	int counter=0;
	for(int i=0;i<this->persons.size();i++){
        if(x!=this->persons.at(i)->getX() && y!=this->persons.at(i)->getY() && this->persons.at(i)->isWearingMask()){
			counter+=((x-this->persons.at(i)->getX())*(x-this->persons.at(i)->getX())+(y-this->persons.at(i)->getY())*(y-this->persons.at(i)->getY())<(int)((this->D+this->persons.at(i)->getRadius())*(this->D+this->persons.at(i)->getRadius()))) ? 1 : 0;
		}
	}
	return counter;
}

bool Comuna::isInsideAnyVac(int xPerson, int yPerson){
    for(int i=0;i<this->vacs.size();i++)
        if(xPerson>this->vacs.at(i)->getX() && xPerson<this->vacs.at(i)->getX()+this->vacs.at(i)->getSize() && yPerson>this->vacs.at(i)->getY() && yPerson<this->vacs.at(i)->getY()+this->vacs.at(i)->getSize()) return true;
    return false;
}

/* Printers */
void Comuna::printState(){
    cout << this->S << ",\t" << this->I << ",\t" << this->R << endl;
}

/* Initters */
void Comuna::initPersons(double speed, double dTheta,int radius,int N,int I,double Itime,double p0,double p1,double p2,double D,double M,bool showD){
    this->S=N-I;
    this->I=I;
    this->R=0;
    this->V=0;
    this->ownSimTime=0;
    if(this->persons.size()!= 0){
        for(int i=0; i<this->persons.size();i++){
            delete this->persons.at(i);
        }
    }
    this->persons.clear();
    Individuo* person=NULL;
    int state;
    bool wearingMask;
    while(this->persons.size()<N){
        state = (this->persons.size()<I) ? INFECTADO: SUSCEPTIBLE;
        wearingMask = ((this->persons.size()<I*M) || (this->persons.size()>=I && this->persons.size()<I+S*M)) ? true : false;
        person = new Individuo(this->width,this->length,speed,dTheta,radius,state,Itime,p0,p1,p2,D,wearingMask,showD);
        this->persons.append(person);
        this->addItem(person);
    }
    chart->axes(Qt::Vertical).first()->setRange(0,N);
}

void Comuna::initVacs(int NumVac,int VacSize){
    Vacunatorio* vac = NULL;
    while(this->vacs.size()<NumVac){
        vac = new Vacunatorio(this->width,this->length,VacSize);
        this->vacs.append(vac);
        this->addItem(vac);
    }
}

void Comuna::deleteVacs(){
    if(this->vacs.size()!= 0)
        for(int i=0; i<this->vacs.size();i++)
            delete this->vacs.at(i);
    this->vacs.clear();
}

void Comuna::clearData(){
    this->serieS->clear();
    this->serieI->clear();
    this->serieR->clear();
    this->serieV->clear();
}

/* Updaters */
void Comuna::updateProbs(double p0,double p1,double p2){
    for(int i=0;i<this->persons.size();i++)
        this->persons.at(i)->updateProbs(p0,p1,p2);
}
