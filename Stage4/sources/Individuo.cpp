/* C++ libs */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;

/* Dev libs */
#include "Individuo.hpp"

/* Constructor */
Individuo::Individuo(int comunaWidth,int comunaLength,double speed,double dTheta,int radius,
                     int state,double Itime,double p0,double p1,double p2,double D,bool wearingMask,bool showD){
    this->comunaWidth = comunaWidth;
    this->comunaLength = comunaLength;
    this->x = radius+rand()%(comunaWidth-2*radius);
    this->y = radius+rand()%(comunaLength-2*radius);
	this->speed = (0.9+0.2*rand()/RAND_MAX)*speed;
	this->dTheta = dTheta;
    this->theta = 2.0*3.14*rand()/RAND_MAX;
    this->radius = radius;
    this->state=state;
    this->stateNext=state;
    this->Itime = (int)(1000*Itime);
    this->initItime = (this->state==INFECTADO) ? 0: INT_MAX;
    this->p0=p0;
    this->p1=p1;
    this->p2=p2;
    this->D=D;
	this->wearingMask=wearingMask;
    this->showD=showD;
}

/* Destructor */
Individuo::~Individuo(){}

/* Getters */
int Individuo::getX(){return this->x;}

int Individuo::getY(){return this->y;}

double Individuo::getSpeed(){return this->speed;}

double Individuo::getDtheta(){return this->dTheta;}

double Individuo::getTheta(){return this->theta;}

int Individuo::getRadius(){return this->radius;}

int Individuo::getState(){return this->state;}

double Individuo::getP0(){return this->p0;}

/* Setters */
void Individuo::setSpeed(double speed){this->speed=speed;}

void Individuo::setRadius(int radius){this->radius=radius;}

/* Printers */
void Individuo::printAll(){
	cout << "Printing person values..." << endl;
	cout << "x\t" << this->x << endl;
	cout << "y\t" << this->y << endl;
	cout << "speed\t" << this->speed << endl;
	cout << "dTheta\t" << this->dTheta << endl;
	cout << "theta" << this->theta << endl << endl;
}

/* State related */
void Individuo::computeNextPos(){
	double thetaNextTemp = this->theta+(2.0*rand()/RAND_MAX-1.0)*this->dTheta;
	int xNextTemp = (int)(this->x+(this->speed)*cos(thetaNextTemp));
	int yNextTemp = (int)(this->y+(this->speed)*sin(thetaNextTemp));
    if(xNextTemp<this->radius){
        xNextTemp = 2*this->radius-xNext;
        thetaNextTemp = 3.14-thetaNextTemp;
	}
    if(xNextTemp>this->comunaWidth-this->radius){
        xNextTemp = 2*(this->comunaWidth-this->radius)-xNextTemp;
        thetaNextTemp = 3.14-thetaNextTemp;
	}
    if(yNextTemp<this->radius){
        yNextTemp = 2*this->radius-yNextTemp;
		thetaNextTemp = -thetaNextTemp;
	}
    if(yNextTemp>this->comunaLength-this->radius){
        yNextTemp = 2*(this->comunaLength-this->radius)-yNextTemp;
		thetaNextTemp = -thetaNextTemp;
	}
	this->xNext = xNextTemp;
	this->yNext = yNextTemp;
    this->thetaNext = thetaNextTemp;//remainder(thetaNextTemp,2.0*M_PI);
}

void Individuo::computeNextState(int simTime,int infectedNum,int maskNum,bool isInsideVac){
    if(this->state==INFECTADO){
        this->stateNext = (simTime-this->initItime > this->Itime) ? RECUPERADO : INFECTADO;
        return;
    }
    if(this->state==SUSCEPTIBLE){
        if(isInsideVac) this->stateNext=VACUNADO;
        else{
            double pTemp;
            this->stateNext=SUSCEPTIBLE;
            for(int i=0;i<infectedNum;i++){
                if(i<maskNum)
                    pTemp = (this->wearingMask) ? this->p2 : this->p1;
                else
                    pTemp = (this->wearingMask) ? this->p1 : this->p0;
                this->stateNext=(1.0*rand()/RAND_MAX<pTemp) ? INFECTADO : this->stateNext;
            }
		}
    }
}

void Individuo::updatePos(){
	this->x = this->xNext;
	this->y = this->yNext;
    this->theta = this->thetaNext;
}

void Individuo::updateState(int simTime){
    if(this->state==SUSCEPTIBLE && this->stateNext==INFECTADO)
        this->initItime=simTime;
    this->state=this->stateNext;
}

/* Overrides */
QRectF Individuo::boundingRect()const{return QRectF(-this->radius,-this->radius,2*this->radius,2*this->radius);}

void Individuo::paint(QPainter* painter,const QStyleOptionGraphicsItem*,QWidget*){
    QColor color;
    switch(this->state){
        case(SUSCEPTIBLE):
            color.setRgb(0,255,0);//green
            break;
        case(INFECTADO):
            color.setRgb(255,0,0);//red
            break;
        case(RECUPERADO):
            color.setRgb(200,100,0);//brown
            break;
        case(VACUNADO):
            color.setRgb(255,255,0);//brown
            break;
    }
	if(this->wearingMask){
		QPen tempPen;
		tempPen.setWidth(2);
		painter->setPen(tempPen);
	}
	else
		painter->setPen(color);
    painter->setBrush(color);
    painter->drawEllipse(this->x-this->comunaWidth-this->radius,this->y-this->comunaLength/2-this->radius,2*this->radius,2*this->radius);
    if(this->state==SUSCEPTIBLE && this->showD){
        color.setAlpha(0);
        painter->setBrush(color);
        painter->setPen(Qt::DashLine);
        painter->drawEllipse(this->x-this->comunaWidth-this->D,this->y-this->comunaLength/2-this->D,2*this->D,2*this->D);
    }
}

/* Booleans */
bool Individuo::isWearingMask(){return this->wearingMask;}

/* Updaters */
void Individuo::updateProbs(double p0,double p1,double p2){
    this->p0=p0;
    this->p1=p1;
    this->p2=p2;
}
