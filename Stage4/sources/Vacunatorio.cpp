/* C/C++ libs */
#include <cmath>

/* Dev libs */
#include "Vacunatorio.hpp"

/* Constructor */
Vacunatorio::Vacunatorio(int comunaWidth,int comunaLength,int size){
    this->comunaWidth=comunaWidth;
    this->comunaLength=comunaLength;
    this->size=size;
    this->x=rand()%(comunaWidth-size);
    this->y=rand()%(comunaLength-size);
}

/* Destructor */
Vacunatorio::~Vacunatorio(){}

/* Getters */
int Vacunatorio::getX(){return this->x;}

int Vacunatorio::getY(){return this->y;}

int Vacunatorio::getSize(){return this->size;}

/* Booleans */
bool Vacunatorio::isInside(int xPerson,int yPerson){return xPerson>this->x && xPerson>this->x+this->size && yPerson>this->y && yPerson>this->y+this->size;}

/* Overrides */
QRectF Vacunatorio::boundingRect() const{return QRectF();}

void Vacunatorio::paint(QPainter *painter,const QStyleOptionGraphicsItem*,QWidget*){
    QColor color(0,0,100,100);
    painter->setBrush(color);
    painter->drawRect(this->x-this->comunaWidth,this->y-this->comunaLength/2,this->size,this->size);
}
