/* Qt libs */
#include <QMenuBar>
#include <QChartView>

/* C++ libs */
#include <iostream>
#include <ctime>
#include <cmath>
#include <fstream>
using namespace std;

/* Dev libs */
#include "Simulator.hpp"
#include "ConfigFile.hpp"
#include "Comuna.hpp"

#define PRINT_ENABLED false

/* Constructor */
Simulator::Simulator(QWidget* parent,ConfigFile* config):QMainWindow(parent),config(config){
    this->setupView();
    this->setupMenuBar();
    this->firstTimeSim = true;
    this->isOn = false;
    this->vacsOn = false;
    this->setFocusPolicy(Qt::ClickFocus);
}

/* Destructor */
Simulator::~Simulator(){
	if(this->config!=NULL){delete this->config;}
	if(this->valpo!=NULL){delete this->valpo;}
	if(this->controlMenu!=Q_NULLPTR){delete this->controlMenu;}
	if(this->startAction!=Q_NULLPTR){delete this->startAction;}
	if(this->stopAction!=Q_NULLPTR){delete this->stopAction;}
	if(this->settingsAction!=Q_NULLPTR){delete this->settingsAction;}
    if(this->timer!=Q_NULLPTR){delete this->timer;}
    if(this->view!=Q_NULLPTR){delete this->view;}
}

/* Setups */
void Simulator::setupMenuBar(){
	this->setWindowTitle("Simulador Gráfico Pandemia 2021");
    this->setFixedSize(2*this->config->getWidth(),this->config->getLength()+this->menuBar()->height()+10);
	this->controlMenu = new QMenu("Control",this);
	this->startAction = new QAction("Start",this);
	this->stopAction = new QAction("Stop",this);
    this->settingsAction = new QAction("Settings",this);
	this->controlMenu->addAction(this->startAction);
	this->controlMenu->addAction(this->stopAction);
	this->menuBar()->addMenu(this->controlMenu);
	this->menuBar()->addAction(this->settingsAction);
    this->timer = new QTimer(this);
    this->timer->setInterval((int)(1000*this->config->getDt()));
    /* Connections to MenuBar */
    connect(this->startAction,SIGNAL(triggered()),this,SLOT(initSlot()));
    connect(this->timer,SIGNAL(timeout()),this,SLOT(startSlot()));
    connect(this->stopAction,SIGNAL(triggered()),this->timer,SLOT(stop()));
    connect(this->stopAction,SIGNAL(triggered()),this,SLOT(stopSlot()));
    connect(this->settingsAction,SIGNAL(triggered()),this,SLOT(settingsSlot()));
    /* Connections to Config Popup */
    connect(this->config,SIGNAL(accepted()),this,SLOT(updateSimConfig()));
}

void Simulator::setupView(){
    this->valpo = new Comuna(this,this->config->getWidth(),this->config->getLength(),this->config->getN(),this->config->getI(),this->config->getD(),this->config->getDt());
    this->valpo->setSceneRect(-this->config->getWidth(),-this->config->getLength()/2,2*this->config->getWidth(),this->config->getLength());
    this->valpo->addRect(-this->config->getWidth(),-this->config->getLength()/2,this->config->getWidth(),this->config->getLength());
    this->view = new QGraphicsView(this->valpo,this);
    this->view->setFixedSize(2*this->config->getWidth()+20,this->config->getLength()+15);
    this->view->setRenderHint(QPainter::Antialiasing);
    this->view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->setCentralWidget(this->view);
}


/* State related */
void Simulator::computeNextSimState(){
    this->valpo->computeNextState();
}

void Simulator::updateSimState(){
    this->valpo->updateState(this->simTime);
	this->simTime += (int)(1000*this->config->getDt());
}

/* Printers */
void Simulator::printSimState(){
    cout << this->simTime << ",\t";
    this->valpo->printState();
}

/* Slots */
void Simulator::initSlot(){
    if((!this->isOn) && this->config->isHidden()){
        connect(this->startAction,SIGNAL(triggered()),this->timer,SLOT(start()));
        this->isOn = true;
        if(this->firstTimeSim){
            this->firstTimeSim = false;
			this->simTime = 0;
            if(PRINT_ENABLED) cout << "t[ms],\t" << "S,\t" << "I,\t" << "R" << "V" << endl;
        }
        this->valpo->initPersons(this->config->getSpeed(),this->config->getDtheta(),this->config->getRadius(),this->config->getN(),this->config->getI(),this->config->getItime(),this->config->getP0(),this->config->getP1(),this->config->getP2(),this->config->getD(),this->config->getM(),this->config->isShowingD());
        this->valpo->deleteVacs();
        this->valpo->clearData();
        this->timer->start();
        if(PRINT_ENABLED) this->printSimState();
        this->data="";
        this->data+=QString::number(this->simTime);
        this->data+=",";
        this->data+=this->valpo->getDataQString();
    }
}

void Simulator::startSlot(){
    if(this->config->isHidden()){
        if(!this->vacsOn && this->valpo->getOwnSimTime()>(int)(1000*this->config->getVactime())){
            this->vacsOn = true;
            this->valpo->initVacs(this->config->getNumvac(),this->config->getVacsize());
        }
        this->computeNextSimState();
        this->updateSimState();
        if(PRINT_ENABLED) this->printSimState();
        this->data+=QString::number(this->simTime);
        this->data+=",";
        this->data+=this->valpo->getDataQString();
    }
}

void Simulator::stopSlot(){
    this->isOn = false;
    this->vacsOn = false;
    this->simTime=0;
    ofstream outFile;
    outFile.open("sources/output.csv",ofstream::trunc);
    outFile << "t[ms],S,I,R,V\n";
    outFile << this->data.toStdString();
    outFile.close();
    this->data="";
}

void Simulator::settingsSlot(){
    if(!this->isOn){
        disconnect(this->startAction,SIGNAL(triggered()),this->timer,SLOT(start()));
        this->config->show();
    }
}

void Simulator::updateSimConfig(){
    this->config->updateSpinnerValues();
}

void Simulator::closeEvent(QCloseEvent*){if(!this->data.isEmpty()) this->stopSlot();}

void Simulator::keyPressEvent(QKeyEvent* event){
    if(this->isOn){
        double factor = 1.0;
        if(event->key()==Qt::Key_Right) factor = 2;
        if(event->key()==Qt::Key_Left) factor = 0.5;
        if(factor!=1.0){
            this->config->setDt(this->config->getDt()/factor);
            this->config->updateP0(factor);
            this->config->updateP1(factor);
            this->config->updateP2(factor);
            this->valpo->updateProbs(this->config->getP0(),this->config->getP1(),this->config->getP2());
            this->timer->setInterval((int)(1000*this->config->getDt()));
        }
    }
}
