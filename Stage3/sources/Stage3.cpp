/* Qt libs */
#include <QApplication>
#include <QMainWindow>

/* C++ libs */
#include <iostream>
#include <cstdlib>

/* Dev libs */
#include "ConfigFile.hpp"
#include "Simulator.hpp"

using namespace std;

int main(int argc,char** argv){
	if(argc==1){
        cerr << "Error (usage): ./Stage3 <configurationFile.txt>" << endl;
		exit(1);
	}
	QApplication app(argc,argv);//Main app
	ConfigFile* config = new ConfigFile(argv);//Configuration file handler
    Simulator sim(Q_NULLPTR,config);//Main window
	sim.show();//Show the Main Window
	return app.exec();
}
