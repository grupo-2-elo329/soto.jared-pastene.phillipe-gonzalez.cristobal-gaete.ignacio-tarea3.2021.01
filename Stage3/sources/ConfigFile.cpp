/* C++ libs */
#include <iostream>
#include <fstream>

/* Dev libs */
#include "ConfigFile.hpp"

#define IS_SHOWING_D false

/*
N	I	I_time
width	length
speed	dt	dtheta
d	M	p0	p1	p2
NumVac	VacSize	VacTime
*/

using namespace std;

ConfigFile::ConfigFile(char** argv){
	ifstream f;
	f.open(argv[1]);
	f >> this->N;
	f >> this->I;
	f >> this->ITIME;
	f >> this->WIDTH;
	f >> this->LENGTH;
	f >> this->SPEED;
	f >> this->DT;
	f >> this->DTHETA;
	f >> this->D;
	f >> this->M;
	f >> this->P0;
	f >> this->P1;
	f >> this->P2;
	f >> this->NUMVAC;
	f >> this->VACSIZE;
	f >> this->VACTIME;
	f.close();
	this->RADIUS=7;
	this->showD=IS_SHOWING_D;
    this->setupWindow();
}

ConfigFile::~ConfigFile(){
    if(this->mainLayout!=Q_NULLPTR) delete this->mainLayout;
}

void ConfigFile::printAll(){
	cout << "Printing config parameters..." << endl;
	cout << "N\t" << this->N << endl;
	cout << "I\t" << this->I << endl;
	cout << "ITIME\t" << this->ITIME << endl;
	cout << "WIDTH\t" << this->WIDTH << endl;
	cout << "LENGTH\t" << this->LENGTH << endl;
	cout << "SPEED\t" << this->SPEED << endl;
	cout << "DT\t" << this->DT << endl;
	cout << "DTHETA\t" << this->DTHETA << endl;
	cout << "D\t" << this->D << endl;
	cout << "M\t" << this->M << endl;
	cout << "P0\t" << this->P0 << endl;
	cout << "P1\t" << this->P1 << endl;
	cout << "P2\t" << this->P2 << endl;
	cout << "NUMVAC\t" << this->NUMVAC << endl;
	cout << "VACSIZE\t" << this->VACSIZE << endl;
	cout << "VACTIME\t" << this->VACTIME << endl << endl;
}

/* Getters */
int ConfigFile::getN(){return this->N;}

int ConfigFile::getI(){return this->I;}

double ConfigFile::getItime(){return this->ITIME;}

int ConfigFile::getWidth(){return this->WIDTH;}

int ConfigFile::getLength(){return this->LENGTH;}

double ConfigFile::getSpeed(){return this->SPEED;}

double ConfigFile::getDt(){return this->DT;}

double ConfigFile::getDtheta(){return this->DTHETA;}

double ConfigFile::getD(){return this->D;}

double ConfigFile::getM(){return this->M;}

double ConfigFile::getP0(){return this->P0;}

double ConfigFile::getP1(){return this->P1;}

double ConfigFile::getP2(){return this->P2;}

int ConfigFile::getNumvac(){return this->NUMVAC;}

double ConfigFile::getVacsize(){return this->VACSIZE;}

double ConfigFile::getVactime(){return this->VACTIME;}

int ConfigFile::getRadius(){return this->RADIUS;}

/* Booleans */
bool ConfigFile::isShowingD(){return this->showD;}

/* Setters */

void ConfigFile::setN(int N){this->N=N;}

void ConfigFile::setI(int I){this->I=I;}

void ConfigFile::setItime(double ITIME){this->ITIME=ITIME;}

void ConfigFile::setWidth(int WIDTH){this->WIDTH=WIDTH;}

void ConfigFile::setLength(int LENGTH){this->LENGTH=LENGTH;}

void ConfigFile::setSpeed(double SPEED){this->SPEED=SPEED;}

void ConfigFile::setDt(double DT){this->DT=DT;}

void ConfigFile::setDtheta(double DTHETA){this->DTHETA=DTHETA;}

void ConfigFile::setD(double D){this->D=D;}

void ConfigFile::setM(double M){this->M=M;}

void ConfigFile::setP0(double P0){this->P0=P0;}

void ConfigFile::setP1(double P1){this->P1=P1;}

void ConfigFile::setP2(double P2){this->P2=P2;}

void ConfigFile::setNumvac(int NUMVAC){this->NUMVAC=NUMVAC;}

void ConfigFile::setVacsize(double VACSIZE){this->VACSIZE=VACSIZE;}

void ConfigFile::setVactime(double VACTIME){this->VACTIME=VACTIME;}

/* Setups */
void ConfigFile::setupWindow(){
	this->setWindowTitle("Settings");
	this->setFixedSize(330,250);
	this->mainLayout = new QFormLayout(this);
	this->mainLayout->setFormAlignment(Qt::AlignCenter);
	this->NSpinner = new QSpinBox();
	this->NSpinner->setMaximum(150);
	this->NSpinner->setValue(this->N);
	this->ISpinner = new QSpinBox();
	this->ISpinner->setValue((int)(this->I));
	this->ISpinner->setMaximum(150);
	this->ITimeSpinner = new QSpinBox();
	this->ITimeSpinner->setValue((int)(this->ITIME));
	this->showDCheckBox = new QCheckBox();
	this->showDCheckBox->setCheckState((this->showD) ? Qt::Checked : Qt::Unchecked);
	this->mainLayout->addRow("Población (N) (máx 150)",this->NSpinner);
	this->mainLayout->addRow("Infectados (I) (máx 150)",this->ISpinner);
	this->mainLayout->addRow("Tiempo infectado (I_time) [s]",this->ITimeSpinner);
	this->mainLayout->addRow("¿Mostrar distancia de contagio (D)?",this->showDCheckBox);
	this->okButton = new QPushButton("OK",this);
	this->mainLayout->addWidget(this->okButton);
	connect(this->okButton,SIGNAL(clicked()),this,SLOT(accept()));
}

/* Updaters */
void ConfigFile::updateSpinnerValues(){
    this->N = this->NSpinner->value();
    this->I = this->ISpinner->value();
	this->ITIME = (double)(this->ITimeSpinner->value());
	this->showD = this->showDCheckBox->checkState() == Qt::Checked;
}
