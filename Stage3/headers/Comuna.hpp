#ifndef COMUNA_HPP
#define COMUNA_HPP
/* Qt libs */
#include <QGraphicsScene>
#include <QList>
#include <QChart>
#include <QAreaSeries>
#include <QLineSeries>
#include <QChartView>
using namespace QtCharts;

/* Dev libs */
#include "Individuo.hpp"

class Comuna : public QGraphicsScene{
    Q_OBJECT
	private:
		int width,length;
        int S,I,R;
        double D;
        QList<Individuo*> persons;
		QLineSeries *serieS, *serieI, *serieR, *serieV;
		QAreaSeries *areaS, *areaI, *areaR, *areaV;
        QChart *chart;
        QChartView *chartView;

	public:
		Comuna(QObject*,int,int,int,int,double);
		~Comuna();
		int getWidth();
        int getLength();
		QString getDataQString();
        /* State related */
        void computeNextState(int);
        void updateState(int);
        int infectedRadar(int,int);
		int maskRadar(int,int);
        /* Updaters */
        void updatePersonsN(double);
        void updatePersonsInfected(int);
		void initPersons(double,double,int,int,int,double,double,double,double,double,double,double,bool);
        /* Printers */
        void printState();
		void clearData();
};

#endif
