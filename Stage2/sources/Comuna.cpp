/* Qt libs */
#include <QRect>

/* C++ libs */
#include <iostream>
#include <ctime>
using namespace std;

/* Dev libs */
#include "Comuna.hpp"

/* Constructor */
Comuna::Comuna(QObject* parent,int width,int length,int N,int I,double D):QGraphicsScene(parent){
    srand(time(NULL));
	this->width=width;
	this->length=length;
    this->S=N-I;
    this->I=I;
    this->R=0;
    this->D=D;
}

/* Destructor */
Comuna::~Comuna(){}

/* Getters */
int Comuna::getWidth(){return this->width;}

int Comuna::getLength(){return this->length;}

QString Comuna::getDataQString(){
    QString result = "";
    result+=QString::number(this->S);
    result+=",";
    result+=QString::number(this->I);
    result+=",";
    result+=QString::number(this->R);
    result+="\n";
    return result;
}

/* State related */
void Comuna::computeNextState(int simTime){
    int xTemp,yTemp;
    for(int i=0;i<this->persons.size();i++){
        this->persons.at(i)->computeNextPos();
        xTemp = this->persons.at(i)->getX();
        yTemp = this->persons.at(i)->getY();
        this->persons.at(i)->computeNextState(simTime,this->infectedRadar(xTemp,yTemp));
    }
}

void Comuna::updateState(int simTime){
    this->S=0;
    this->I=0;
    this->R=0;
    for(int i=0;i<this->persons.size();i++){
        this->persons.at(i)->updatePos();
        this->persons.at(i)->updateState(simTime);
        this->S+=(this->persons.at(i)->getState()==SUSCEPTIBLE)? 1 : 0;
        this->I+=(this->persons.at(i)->getState()==INFECTADO)? 1 : 0;
        this->R+=(this->persons.at(i)->getState()==RECUPERADO)? 1 : 0;
    }
    this->update(-this->width/2,-this->length/2,this->width,this->length);
}

int Comuna::infectedRadar(int x, int y){
    int counter=0;
    for(int i=0;i<this->persons.size();i++){
        if(x!=this->persons.at(i)->getX() and y!=this->persons.at(i)->getY() and this->persons.at(i)->getState()==INFECTADO){
            counter+=((x-this->persons.at(i)->getX())*(x-this->persons.at(i)->getX())+(y-this->persons.at(i)->getY())*(y-this->persons.at(i)->getY())<(int)((this->D+this->persons.at(i)->getRadius())*(this->D+this->persons.at(i)->getRadius()))) ? 1 : 0;
        }
    }
    return counter;
}

/* Updaters */
void Comuna::updatePersonsSpeed(double speed){
    for(int i=0;i<this->persons.size();i++)
        this->persons.at(i)->setSpeed(speed);
}

void Comuna::updatePersonsRadius(int radius){
    for(int i=0;i<this->persons.size();i++)
        this->persons.at(i)->setRadius(radius);
}

/* Printers */
void Comuna::printState(){
    cout << this->S << ",\t" << this->I << ",\t" << this->R << endl;
}

/* Initters */
void Comuna::initPersons(double speed, double dTheta,int radius,int N,int I,double Itime,double p0,double p1,double p2,double D,double Dt,bool showD){
    this->S=N-I;
    this->I=I;
    this->R=0;
    if(this->persons.size()!= 0)
        for(int i=0; i<this->persons.size();i++)
            delete this->persons.at(i);
    this->persons.clear();
    Individuo* person=NULL;
    int state;
    while(this->persons.size()<N){
        state = (this->persons.size()<I) ? INFECTADO: SUSCEPTIBLE;
        person = new Individuo(this->width,this->length,speed,dTheta,radius,state,Itime,p0,p1,p2,D,Dt,showD);
        this->persons.append(person);
        this->addItem(person);
    }
}
