#ifndef CONFIGFILE_HPP
#define CONFIGFILE_HPP

#include <QDialog>
#include <QFormLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QCheckBox>

class ConfigFile : public QDialog{
    Q_OBJECT
	private:
		int N;
		int I;
		double ITIME;
		int WIDTH;
		int LENGTH;
		double SPEED;
		double DT;
		double DTHETA;
		double D;
		double M;
		double P0;
		double P1;
		double P2;
		int NUMVAC;
		double VACSIZE;
		double VACTIME;
        int RADIUS;
        bool showD;
        QFormLayout* mainLayout;
        QPushButton* okButton;
        QSpinBox* radiusSpinner;
        QSpinBox* speedSpinner;
        QCheckBox* showDCheckBox;

	public:
		ConfigFile(char**);
		~ConfigFile();
		/* Getters */
		int getN();
		int getI();
		double getItime();
		int getWidth();
		int getLength();
		double getSpeed();
		double getDt();
		double getDtheta();
		double getD();
		double getM();
		double getP0();
		double getP1();
		double getP2();
		int getNumvac();
		double getVacsize();
		double getVactime();
        int getRadius();
        /* Booleans */
        bool isShowingD();
        /* Printers */
		void printAll();
		/* Setters */
		void setN(int);
		void setI(int);
		void setItime(double);
		void setWidth(int);
		void setLength(int);
		void setSpeed(double);
		void setDt(double);
		void setDtheta(double);
		void setD(double);
		void setM(double);
		void setP0(double);
		void setP1(double);
		void setP2(double);
		void setNumvac(int);
		void setVacsize(double);
		void setVactime(double);
        /* Setupers */
        void setupWindow();
        /* Updaters */
        void updateSpinnerValues();
};

#endif
