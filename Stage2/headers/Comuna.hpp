#ifndef COMUNA_HPP
#define COMUNA_HPP
/* Qt libs */
#include <QGraphicsScene>
#include <QList>

/* Dev libs */
#include "Individuo.hpp"

class Comuna : public QGraphicsScene{
    Q_OBJECT
	private:
		int width,length;
        int S,I,R;
        double D;
        QList<Individuo*> persons;
	public:
        Comuna(QObject*,int,int,int,int,double);
		~Comuna();
		int getWidth();
		int getLength();
        QString getDataQString();
        /* State related */
        void computeNextState(int);
        void updateState(int);
        int infectedRadar(int,int);
        /* Updaters */
        void updatePersonsSpeed(double);
        void updatePersonsRadius(int);
        /* Printers */
        void printState();
        /* Initters */
        void initPersons(double,double,int,int,int,double,double,double,double,double,double,bool);
};

#endif
